#!/bin/sh
#
# Copyright ©2014-2017 by Sharon L. Krossa (skrossa@sharonkrossa.com) and
# Robert A. Black (robert.black@sonic.net)
# 
# This file is part of Make-live-test-dev-scripts.
# 
#     Make-live-test-dev-scripts is free software: you can redistribute
#     it and/or modify it under the terms of the GNU General Public
#     License as published by the Free Software Foundation, either
#     version 3 of the License, or any later version.
# 
#     Make-live-test-dev-scripts is distributed in the hope that it will
#     be useful, but WITHOUT ANY WARRANTY; without even the implied
#     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#     See the GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Make-live-test-dev-scripts.  If not, see
#     <http://www.gnu.org/licenses/>.
#     
#######################################################################
#
#  Writes a Drush alias file for the current Drupal directory into a
#  .drush directory in the specified (usually the users home) directory.
#
#  This script should be run from within the relevant Drupal directory.
#
#
########################################################################
# Set variables for easy color inclusion in echos                      #
########################################################################
# from Wicked Cool Shell Scripts written by Dave Taylor
# http://www.intuitive.com/wicked/showscript.cgi?011-colors.sh
# ANSI Color -- use these variables to easily have different color
#    and format output. Make sure to output the reset sequence after
#    colors (f = foreground, b = background), and use the 'off'
#    feature for anything you turn on.

initializeANSI()
{
# apparently the funny character esc is set to works when run
  esc=""

  blackf="${esc}[30m";   redf="${esc}[31m";    greenf="${esc}[32m"
  yellowf="${esc}[33m"   bluef="${esc}[34m";   purplef="${esc}[35m"
  cyanf="${esc}[36m";    whitef="${esc}[37m"

  blackb="${esc}[40m";   redb="${esc}[41m";    greenb="${esc}[42m"
  yellowb="${esc}[43m"   blueb="${esc}[44m";   purpleb="${esc}[45m"
  cyanb="${esc}[46m";    whiteb="${esc}[47m"

  boldon="${esc}[1m";    boldoff="${esc}[22m"
  italicson="${esc}[3m"; italicsoff="${esc}[23m"
  ulon="${esc}[4m";      uloff="${esc}[24m"
  invon="${esc}[7m";     invoff="${esc}[27m"

  reset="${esc}[0m"
}

initializeANSI

##########################################################################
# Create Live, Test, or Dev alias                                        #
##########################################################################
#
# List existing drush aliases
echo "${greenf}Existing Drush alises. Note: if intending to replace one of these, the existing alias file must be deleted first. ${reset}"
drush sa

# Name of core folder for Drupal installation
# (e.g., something indicative of the site,
# such as "mlb" for Mission Ladies Basketball".
if [ "$1" ]; then
  drupalDirName=$1
else
  echo "${bluef}Enter label for the Drupal site (omit -live, -test, -dev designators): ${reset}"
  read drupalDirName
fi

if [ ! ${drupalDirName} ]; then
  echo "${redf}No label entered. Exiting. ${reset}"
  exit
fi

if [ "$2" ]; then
  liveTestDev=$2
else
  echo "${bluef}Enter live, test, or dev (all lower case): ${reset}"
  read liveTestDev
fi

if [ ${liveTestDev} ]; then
#if [ ${liveTestDev} = "live" || ${liveTestDev} = "test" || ${liveTestDev} = "dev" ]; then
  echo "${greenf}Okay. Continuing for ${liveTestDev}. ${reset}"
else
  echo "${redf}Sorry, live, test, dev not entered. Exiting. ${reset}"
  exit
fi

thisDir=`pwd`

homeDir="$HOME"

if [ -f "${homeDir}/.drush/${drupalDirName}${liveTestDev}.alias.drushrc.php" ] ; then
  echo "${redf}File ${homeDir}/.drush/${drupalDirName}${liveTestDev}.alias.drushrc.php already exists. Exiting. ${reset}"
  exit
fi

echo "${bluef}Will be creating the alias file in ${homeDir}/.drush/${drupalDirName}${liveTestDev}.alias.drushrc.php. Proceed? (y/n)${reset}"
read response
if [ ${response} = "y" ]; then
  echo "${greenf}Okay. Continuing. ${reset}"
else
  echo "${redf}Okay. Exiting. ${reset}"
  exit
fi



echo "<?php" > ${homeDir}/.drush/${drupalDirName}${liveTestDev}.alias.drushrc.tmp
echo `drush site-alias @self` >> ${homeDir}/.drush/${drupalDirName}${liveTestDev}.alias.drushrc.tmp


sed "s%self%${drupalDirName}${liveTestDev}%g" <${homeDir}/.drush/${drupalDirName}${liveTestDev}.alias.drushrc.tmp >${homeDir}/.drush/${drupalDirName}${liveTestDev}.alias.drushrc.php

rm ${homeDir}/.drush/${drupalDirName}${liveTestDev}.alias.drushrc.tmp
