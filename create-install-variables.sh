#!/bin/sh
#
# Copyright ©2014-2017 by Sharon L. Krossa (skrossa@sharonkrossa.com)
# and Robert A. Black (robert.black@sonic.net)
# 
# This file is part of Make-live-test-dev-scripts.
# 
#     Make-live-test-dev-scripts is free software: you can redistribute
#     it and/or modify it under the terms of the GNU General Public
#     License as published by the Free Software Foundation, either
#     version 3 of the License, or any later version.
# 
#     Make-live-test-dev-scripts is distributed in the hope that it will
#     be useful, but WITHOUT ANY WARRANTY; without even the implied
#     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#     See the GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Make-live-test-dev-scripts.  If not, see
#     <http://www.gnu.org/licenses/>.
#     
#######################################################################
#
#  Creates files for variables to use in live, test, dev scripts 
#  to indicate the site label and backup directory.
#
#
########################################################################
# Set variables for easy color inclusion in echos                      #
########################################################################
# from Wicked Cool Shell Scripts written by Dave Taylor
# http://www.intuitive.com/wicked/showscript.cgi?011-colors.sh
# ANSI Color -- use these variables to easily have different color
#    and format output. Make sure to output the reset sequence after
#    colors (f = foreground, b = background), and use the 'off'
#    feature for anything you turn on.

initializeANSI()
{
# apparently the funny character esc is set to works when run
  esc=""

  blackf="${esc}[30m";   redf="${esc}[31m";    greenf="${esc}[32m"
  yellowf="${esc}[33m"   bluef="${esc}[34m";   purplef="${esc}[35m"
  cyanf="${esc}[36m";    whitef="${esc}[37m"

  blackb="${esc}[40m";   redb="${esc}[41m";    greenb="${esc}[42m"
  yellowb="${esc}[43m"   blueb="${esc}[44m";   purpleb="${esc}[45m"
  cyanb="${esc}[46m";    whiteb="${esc}[47m"

  boldon="${esc}[1m";    boldoff="${esc}[22m"
  italicson="${esc}[3m"; italicsoff="${esc}[23m"
  ulon="${esc}[4m";      uloff="${esc}[24m"
  invon="${esc}[7m";     invoff="${esc}[27m"

  reset="${esc}[0m"
}

initializeANSI

##########################################################################
# Create label and backup directory variables                            #
##########################################################################

## Label variable

echo "${bluef}Enter label for the Drupal sites (omit -live, -test, -dev designators): ${reset}"
read drupalLabel

if [ ! ${drupalLabel} ]; then
  echo "${redf}No label entered. Exiting. ${reset}"
  exit
else
  echo "${drupalLabel}" > ./label.var
fi

## Backup directory variable

echo "${bluef}The default backup directory is ${HOME}/drush-backups. Do you want to use a custom backup directory instead? (y/n) ${reset}"
read response

if [ ${response} = "y" ]; then
  echo "${bluef}Enter the absolute path to the backup directory, without a trailing slash (e.g./home/username/drupalbackups): ${reset}"
  read backupDir
  if [ ! ${backupDir} ]; then
    echo "${redf}No directory entered. Exiting. ${reset}"
    exit
  fi
  echo "${backupDir}" > ./backupdir.var
elif [ ${response} = "n" ]; then
  echo "${HOME}/drush-backups" > ./backupdir.var
else
  echo "${redf}Something other than y or n entered. Exiting. ${reset}"
  exit
fi
