Copyright ©2014-2017 by Sharon L. Krossa (skrossa@sharonkrossa.com) and
Robert A. Black (robert.black@sonic.net)

This file is part of Make-live-test-dev-scripts.

    Make-live-test-dev-scripts is free software: you can redistribute it
    and/or modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation, either version 3 of
    the License, or any later version.

    Make-live-test-dev-scripts is distributed in the hope that it will
    be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Make-live-test-dev-scripts.  If not, see
    <http://www.gnu.org/licenses/>.

#######################################################################
#                                                                     #
#           How to create & use the live, test, dev scripts.          #
#                                                                     #
#######################################################################

All commands below are executed in the command line, using a terminal
application.

################################# -I- #################################
I. Install the make-live-test-dev-scripts git repository. (This only
needs to be done once for each new version of
make-live-test-dev-scripts.)

                      ########## A or ###########
  A. If this git repository does not yet exist on the relevant server:

####1. If it does not already exist, create a non-public directory on
    the same server as the Drupal site. For example:

      a. Navigate to your home directory with this command:

          cd ~

      b. Create a directory named "install" in your home directory with
      this command:

          mkdir install

####2. Navigate to the install directory. For example, with this
    command:

        cd ~/install

####3. Clone the make-live-test-dev-scripts git repository with this
    command (all on one line):

        git clone https://skrossa@bitbucket.org/skplus/make-live-test-dev-scripts.git
        
####4. If asked for a password, just press the return/enter key (that
    is, enter a null password).

                    ########## or B ###########
  B. Or, if this git repository already exists on the relevant server:

####1. Navigate to local repository directory. For example, with this
    command:

        cd ~/install/make-live-test-dev-scripts

####2. Pull the latest version of the make-live-test-dev-scripts git
    repository with this command:

        git pull origin

####3. If asked for a password, just press the return/enter key (that
    is, enter a null password).

################################# -II- ################################
II. Use the scripts to make aliases, backup scripts, and copy scripts.
Each Drupal site needs its own scripts for backing up and copying, as
well as its own aliases. (This section only needs to be done once for
each live site for each new version of make-live-test-dev-scripts.)

                    ########## A and ###########
  A. Make the Scripts for the relevant Drupal site. (This only needs to
  be done once for each live site for each new version of
  make-live-test-dev-scripts.)

####1. Navigate into local repository directory. For example, with this
    command:

        cd ~/install/make-live-test-dev-scripts

####2. Make sure the "master" branch is checked out (active) with this
    command:

        git checkout master

####3. Display the current settings for alias site label and backup
    directory with this command:

        make show-config

####4. If the settings are not correct for the relevant Drupal site,
    configure the alias site label and the location of the backup
    directory with this command:

        make config

####5. Create the alias, backup, and copy scripts, which will be placed
    in the bin directory of your home directory (~/bin), with this
    command:

        make install

    The backup and copy scripts installed in ~/bin will each start with
    whatever alias site label was used in step II.A.4, above. For
    example, if the label were "aliaslabel":

      aliaslabel-backup-dev.sh
      aliaslabel-backup-live.sh
      aliaslabel-backup-test.sh
      aliaslabel-copy-dev-to-test.sh
      aliaslabel-copy-live-to-dev.sh
      aliaslabel-copy-live-to-test.sh
      aliaslabel-copy-test-to-dev.sh

    The alias creation script, because it can be used on any site, does
    not start with the alias site label:

      create-drush-alias.sh

                    ########## and B ###########
  B. Make Aliases for the live, test, and dev versions of the relevant
  Drupal site. (This sub-section does NOT normally need to be repeated
  for each new version of make-live-test-dev-scripts.)

####0. If they don't already exist, create a Drupal site each for test
    and dev. See section V below for more information.
    
####1. Navigate to the Drupal directory of the live, test, and dev
    sites for the relevant Drupal site in turn and execute the
    create-drush-alias.sh script for each with this command:

        ~/bin/create-drush-alias.sh

    Use the same alias site label as used when running make config above
    (Step II.A.4.)

####2. Check that all the aliases do indeed exist with this command:

        drush sa

####3. For each alias, check that they go to the right place using
    these commands:

        cd `drush dd @aliaslabellive`
        cd `drush dd @aliaslabeltest`
        cd `drush dd @aliaslabeldev`

    (Replacing "aliaslabel" with the alias site label used in steps
    II.A.4 and II.B.1) Each command should take you to the respective
    Drupal directory for that site.

################################ -III- ################################
III. Use the backup and copy scripts that are now in the ~/bin
directory.

  That's it -- use the copy scripts and backup scripts in ~/bin to copy
  or backup the live, test, and dev sites. (Easiest done by navigating
  to ~/bin first, but can execute them from anywhere using
  ~/bin/whatever…)

################################# -IV- ################################
IV. General principles for how to use test and dev sites:

Different people and teams have different workflows for how they use
their test and dev sites. However, as a starting point, this is how I
make use of them:

**The most important rule: NEVER try something new on your live site!**

Whenever you make a change to your live site, whether adding a new
structure, a new module, a new view or content type, or whatever, you
should know before you touch your live site that what you're about to do
will work without affecting the rest of the site. If in doubt, don't do
it on live!

Instead, always try out new things on either your dev or your test site.

**What's the difference between test and dev?**

There are different approaches people take to this, but until you figure
out what works best for you (or join a team with an established
workflow), I recommend using test to test automated scripts or very
quick and easy changes (that is, where you won't lose much work if
someone accidentally comes by and blows away the test site before you're
done) and using dev to test/develop more complex changes (that is, where
you need everyone else to leave dev alone until you're done). So:

*Test = testing automated or very quick & easy changes 
*Dev = testing or developing complex things over time

The general rule for my teams are: copy from live (or dev) to test
freely at any time, but always ask the rest of the team before copying
from live (or test) to dev.

**Some examples:

*Testing code updates (on Test)

Before applying code updates to live, I first write a shell script and
apply it to test first: 

1. Write/adjust update script 
2. Copy live to test 
3. Run script on text 
4. Repeat steps 1-4 until script works with no problem 
5. Finally, run tested script on live

*Finding out if a module will solve the problem I want it to (on Dev)

Before adding an unfamiliar module (or module I want to use for a new
purpose) to live, I first install it on dev and test it fully there: 

1. Copy live to dev 
2. Install module on dev 
3. Set permissions 
4. Set settings 
5. Try doing or building whatever it is I wanted the module for
6. If it works, great -- repeat successful steps on live; 
   If it does NOT work, no worries -- go back to step 1 with another 
   module(s)

**Why isn't there a script to copy from test or dev TO live?**

Unless you have a complex and non-standard implementation of Drupal (in
which case you won't be using these scripts!), a Drupal site's content
is edited directly on the live site. This means the master copy of a
Drupal site's content is in live, and therefore live should never++ be
overwritten. This is also why all new things need to be tested on test
or dev first. Live isn't just live online, it's the master copy. If
something happens to live, you need to restore from backup.

++There are some exceptions to "never", but they're above the paygrade
of this discussion!

################################# -V- #################################
V. How to create test and dev sites to use with your live site:

For both test and dev, create a new, empty Drupal site, each with their
own database, database user, and password. Remember to make the
passwords not only different, but as long and complicated as you
can--it's not something you're going to have to type in much (if ever)
and it's critical to your site's security! I also recommend database
names that indicate both the site these are for and either "dev" or
"test", as appropriate, so you can tell which database you're dealing
with just from its name. Depending on your system settings, you may not
be able to have very long database names, so try to be clear but short.
For example, one might use "heylive", "heytest" and "heydev" as the
database (and database user) names for a project called "Hey Everybody". 

For Drupal 7, that's all that's needed. For Drupal 8, there is an
additional, one time step, to prepare the test and dev sites to be used
with your live site: we need to make sure the config_directories setting
is the same for all three sites (live, test, and dev). This is needed in
order to take advantage of configuration management in the future.

  1. In your live site drupal directory, open the
  sites/default/settings.php file.

  2. Go down to the very bottom of the text, where you should find a
  line that looks something like this:
  
    $config_directories['sync'] = 'sites/default/files/config_PeqSQy0oo6QyPBqX_Fk7_RDV8eypCO6l6psE1DaCzlQOwmWi1AvohD_Y-EexfluMzLduNEn1hw/sync';

  3. Copy that whole line, starting with the dollar sign ($) and
  including the semicolon (;) 
  
  4.  Edit the settings.php files of both your test and dev sites and
  replace their $config_directories lines with the line you copied from
  live's settings.php 

###End README###
