# Copyright ©2014-2017 by Sharon L. Krossa (skrossa@sharonkrossa.com)
# and Robert A. Black (robert.black@sonic.net)
# 
# This file is part of Make-live-test-dev-scripts.
# 
#     Make-live-test-dev-scripts is free software: you can redistribute
#     it and/or modify it under the terms of the GNU General Public
#     License as published by the Free Software Foundation, either
#     version 3 of the License, or any later version.
# 
#     Make-live-test-dev-scripts is distributed in the hope that it will
#     be useful, but WITHOUT ANY WARRANTY; without even the implied
#     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#     See the GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Make-live-test-dev-scripts.  If not, see
#     <http://www.gnu.org/licenses/>.
#     
#######################################################################
SHELL = /bin/sh

srcdir = .

bindir = ${HOME}/bin

label = $(shell cat label.var)

backupDir = $(shell cat backupdir.var)

variableFiles = label.var backupdir.var

sedFiles = backup-dev.sed backup-live.sed backup-test.sed \
           copy-dev-to-test.sed copy-live-to-dev.sed \
           copy-live-to-test.sed copy-test-to-dev.sed \
           update-dev.sed update-live.sed update-test.sed

backupscripts = $(label)-backup-dev.sh $(label)-backup-live.sh $(label)-backup-test.sh

copyscripts = $(label)-copy-dev-to-test.sh $(label)-copy-live-to-dev.sh \
              $(label)-copy-live-to-test.sh $(label)-copy-test-to-dev.sh

updatescripts = $(label)-update-live.sh $(label)-update-test.sh \
                $(label)-update-dev.sh

scripts = $(backupscripts) $(copyscripts) $(updatescripts)


all:
	@echo 'Configure the alias site label and backup directory using "make config"'
	@echo 'Display current alias site label and backup directory configuration using "make show-config"'
	@echo 'Install alias, backup, copy, and update scripts for $(label) sites in $(DESTDIR)$(bindir) using "make install"'

.PHONY: all config show-config install clean

config:
	sh $(srcdir)/create-install-variables.sh
	
show-config:
	@echo 'Site label = '$(label)
	@echo 'Backup directory = '$(backupDir)

ifeq ($(strip $(label)),)

install:
	@echo 'First you must configure the alias site label and backup directory using "make config"'

else

install: $(scripts)
	mkdir -p $(DESTDIR)$(bindir)
	cp create-drush-alias.sh $(DESTDIR)$(bindir)
	chmod u=rwx $(DESTDIR)$(bindir)/create-drush-alias.sh
	cp $(label)-backup-dev.sh $(DESTDIR)$(bindir)
	chmod u=rwx $(DESTDIR)$(bindir)/$(label)-backup-dev.sh
	cp $(label)-backup-live.sh $(DESTDIR)$(bindir)
	chmod u=rwx $(DESTDIR)$(bindir)/$(label)-backup-live.sh
	cp $(label)-backup-test.sh $(DESTDIR)$(bindir)
	chmod u=rwx $(DESTDIR)$(bindir)/$(label)-backup-test.sh
	cp $(label)-copy-dev-to-test.sh $(DESTDIR)$(bindir)
	chmod u=rwx $(DESTDIR)$(bindir)/$(label)-copy-dev-to-test.sh
	cp $(label)-copy-live-to-dev.sh $(DESTDIR)$(bindir)
	chmod u=rwx $(DESTDIR)$(bindir)/$(label)-copy-live-to-dev.sh
	cp $(label)-copy-live-to-test.sh $(DESTDIR)$(bindir)
	chmod u=rwx $(DESTDIR)$(bindir)/$(label)-copy-live-to-test.sh
	cp $(label)-copy-test-to-dev.sh $(DESTDIR)$(bindir)
	chmod u=rwx $(DESTDIR)$(bindir)/$(label)-copy-test-to-dev.sh
	cp $(label)-update-dev.sh $(DESTDIR)$(bindir)
	chmod u=rwx $(DESTDIR)$(bindir)/$(label)-update-dev.sh
	cp $(label)-update-live.sh $(DESTDIR)$(bindir)
	chmod u=rwx $(DESTDIR)$(bindir)/$(label)-update-live.sh
	cp $(label)-update-test.sh $(DESTDIR)$(bindir)
	chmod u=rwx $(DESTDIR)$(bindir)/$(label)-update-test.sh
	

$(label)-backup-dev.sh: mysite-backup.sh.in backup-dev.sed
	sed -f backup-dev.sed < mysite-backup.sh.in > $(label)-backup-dev.sh

$(label)-backup-live.sh: mysite-backup.sh.in backup-live.sed 
	sed -f backup-live.sed < mysite-backup.sh.in > $(label)-backup-live.sh

$(label)-backup-test.sh: mysite-backup.sh.in backup-test.sed 
	sed -f backup-test.sed < mysite-backup.sh.in > $(label)-backup-test.sh
	
	
$(label)-copy-dev-to-test.sh: mysite-copy.sh.in copy-dev-to-test.sed
	sed -f copy-dev-to-test.sed < mysite-copy.sh.in > $(label)-copy-dev-to-test.sh

$(label)-copy-live-to-dev.sh: mysite-copy.sh.in copy-live-to-dev.sed 
	sed -f copy-live-to-dev.sed < mysite-copy.sh.in > $(label)-copy-live-to-dev.sh

$(label)-copy-live-to-test.sh: mysite-copy.sh.in copy-live-to-test.sed
	sed -f copy-live-to-test.sed < mysite-copy.sh.in > $(label)-copy-live-to-test.sh

$(label)-copy-test-to-dev.sh: mysite-copy.sh.in copy-test-to-dev.sed 
	sed -f copy-test-to-dev.sed < mysite-copy.sh.in > $(label)-copy-test-to-dev.sh


$(label)-update-dev.sh: mysite-update.sh.in update-dev.sed
	sed -f update-dev.sed < mysite-update.sh.in > $(label)-update-dev.sh

$(label)-update-live.sh: mysite-update.sh.in update-live.sed 
	sed -f update-live.sed < mysite-update.sh.in > $(label)-update-live.sh

$(label)-update-test.sh: mysite-update.sh.in update-test.sed 
	sed -f update-test.sed < mysite-update.sh.in > $(label)-update-test.sh


$(sedFiles): variables.sed
	sed -f variables.sed < $@.in > $@

variables.sed: $(variableFiles)
	echo "s%##label##%$(label)%g" > variables.sed
	echo "s%##backupdir##%$(backupDir)%g" >> variables.sed

clean:
	rm -f $(scripts) $(sedFiles) variables.sed
	echo "" > label.var
	echo "" > backupDir.var

endif
